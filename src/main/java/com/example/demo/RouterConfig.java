package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
@EnableWebFlux
public class RouterConfig {
	
	@Bean
	public RouterFunction<ServerResponse> getRoutes(StringHandler stringHandler)	{
		
		return RouterFunctions.route()
				.GET("/test", stringHandler::getAll)
				.POST("/test", stringHandler::addPerson)
				.GET("/test2", stringHandler::getParam)
				.GET("/test/{id}", stringHandler::getPathVariable)
				.build();
		
	}

}
