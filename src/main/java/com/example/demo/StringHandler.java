package com.example.demo;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Mono;

@Component
public class StringHandler {
	
	
	public Mono<ServerResponse> getAll(ServerRequest request)	{
		Mono<Person> personMono = Mono.just(new Person("1", "Hello"));
		return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(personMono, Person.class);
	}
	
	public Mono<ServerResponse> addPerson(ServerRequest request)	{
		Mono<Person> personMono = request.bodyToMono(Person.class);
		return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(personMono, Person.class);
	}
	
	public Mono<ServerResponse> getParam(ServerRequest request)	{
		String personMono = request.queryParam("id").get();
		return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(Mono.just(personMono), Person.class);
	}
	
	public Mono<ServerResponse> getPathVariable(ServerRequest request)	{
		String personMono = request.pathVariable("id");
		return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(Mono.just(personMono), Person.class);
	}

}
